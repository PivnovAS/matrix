#ifndef HEADER_H
#define HEADER_H

template <typename T>
class Matrix
{
public:
    int AmountString, AmountColumn;
    T** data;

    Matrix(int n, int m); //Конструктор

    ~Matrix(); //Деструктор

    T &operator()(int index1, int index2); //Обращение по индексу

    int amountString(); //Получение размера строки

    int amountColumn(); //Получение размера столбца

    void addString(); //Добавить строку

    void delString(); //Удалить строку

    void addColumn(); //Добавить столбец

    void delColumn(); //удалить столбец

    void clear(); //Очистить матрицу

    void show(); //Вывод на экран

    T& submatrix(Matrix<T>& matrix, int NewAmountString, int NewAmountColumn); // Получение подматрицы

    void transposition(); //Транспонирование

    void operator=(const Matrix<T>& matrix);
};


template <typename T>
void Matrix<T>::operator=(const Matrix<T>& matrix)
{
    for (int i=0; i<AmountString; i++)
    for (int j=0; j<AmountColumn; j++)
    {
       data[i][j]=matrix.data[i][j];
    }

    return **result.data;
}

template <typename T>
Matrix<T>::Matrix(int n, int m)
{
    AmountString = n;
    AmountColumn = m;
    data = new T*[AmountString];
    for (int i=0; i<AmountString; i++)
    {
        data[i] = new T[AmountColumn];
    }
}

template <typename T>
Matrix<T>::~Matrix()
{
    AmountString = 0;
    AmountColumn = 0;
    delete[]data;
}

template <typename T>
T& Matrix<T>::operator()(int index1, int index2)
{
    T null;
    if (index1>0 && index2>0 && index1<=AmountString && index2<=AmountColumn)
    {
        return data[index1-1][index2-1];
    }
    else return null;
}

template <typename T>
int Matrix<T>::amountString()
{
    return AmountString;
}

template <typename T>
int Matrix<T>::amountColumn()
{
    return AmountColumn;
}

template <typename T>
void Matrix<T>::addString()
{
    AmountString++;
    T** result = new T*[AmountString];
    for (int i=0; i<AmountString; i++)
    {
        result[i] = new T[AmountColumn];
    }

    for (int i=0; i<AmountString-1; i++)
    for (int j=0; j<AmountColumn; j++)
    {
        result[i][j]=data[i][j];
    }

    delete[] data;

    data = new T*[AmountString];
    for (int i=0; i<AmountString; i++)
    {
         data[i] = new T[AmountColumn];
    }

    for (int i=0; i<AmountString-1; i++)
    for (int j=0; j<AmountColumn; j++)
    {
        data[i][j]=result[i][j];
    }

}

template <typename T>
void Matrix<T>::delString()
{
    if (AmountString>0)
    {
        AmountString--;

        T** result = new T*[AmountString];
        for (int i=0; i<AmountString; i++)
        {
            result[i] = new T[AmountColumn];
        }

        for (int i=0; i<AmountString; i++)
        for (int j=0; j<AmountColumn; j++)
        {
            result[i][j]=data[i][j];
        }

        delete[] data;

        AmountString++;
        data = new T*[AmountString];
        for (int i=0; i<AmountString; i++)
        {
             data[i] = new T[AmountColumn];
        }

        AmountString--;
        for (int i=0; i<AmountString; i++)
        for (int j=0; j<AmountColumn; j++)
        {
            data[i][j]=result[i][j];
        }
    }


}

template <typename T>
void Matrix<T>::addColumn()
{
    AmountColumn++;
    T** result = new T*[AmountString];
    for (int i=0; i<AmountString; i++)
    {
        result[i] = new T[AmountColumn];
    }

    for (int i=0; i<AmountString; i++)
    for (int j=0; j<AmountColumn-1; j++)
    {
        result[i][j]=data[i][j];
    }

    delete[] data;

    data = new T*[AmountString];
    for (int i=0; i<AmountString; i++)
    {
         data[i] = new T[AmountColumn];
    }

    for (int i=0; i<AmountString; i++)
    for (int j=0; j<AmountColumn-1; j++)
    {
        data[i][j]=result[i][j];
    }
}

template <typename T>
void Matrix<T>::delColumn()
{
    if (AmountColumn>0)
    {
        AmountColumn--;
        T** result = new T*[AmountString];
        for (int i=0; i<AmountString; i++)
        {
            result[i] = new T[AmountColumn];
        }

        for (int i=0; i<AmountString; i++)
        for (int j=0; j<AmountColumn; j++)
        {
            result[i][j]=data[i][j];
        }

        delete[] data;

        data = new T*[AmountString];
        for (int i=0; i<AmountString; i++)
        {
             data[i] = new T[AmountColumn];
        }

        for (int i=0; i<AmountString; i++)
        for (int j=0; j<AmountColumn; j++)
        {
            data[i][j]=result[i][j];
        }
    }
}

template <typename T>
void Matrix<T>::clear()
{
    delete[]data;
    data = new T*[AmountString];
    for (int i=0; i<AmountString; i++)
    {
        data[i] = new T[AmountColumn];
    }
}

template <typename T>
void Matrix<T>::show()
{
    for(int i = 0; i<AmountString; i++)
    {
        for(int j = 0; j<AmountColumn; j++)
        {
            cout << data[i][j] << ' ';
        }
        cout << endl;
    }
}

template <typename T>
void Matrix<T>::transposition()
{
    if (AmountColumn=AmountString)
    {
        for (int i = 0; i<AmountString-1; i++)
        for (int j = 1+(i-1); j<AmountColumn; j++)
        {
            T buff;
            buff = data[j][i];
            data[j][i] = data[i][j];
            data[i][j] = buff;
        }
    }
}

template <typename T>
T& Matrix<T>::submatrix(Matrix<T>& matrix, int NewAmountString, int NewAmountColumn)
{
    if (NewAmountString<=AmountString && NewAmountColumn<=AmountColumn && NewAmountString>0 && NewAmountColumn>0)
    {
        AmountString = NewAmountString;
        AmountColumn = NewAmountColumn;

        data = new T*[AmountString];
        for (int i=0; i<AmountString; i++)
        {
            data[i] = new T[AmountColumn];
        }

        for (int i=0; i<AmountString; i++)
        for (int j=0; j<AmountColumn; j++)
        {
            data[i][j]=matrix.data[i][j];
        }

        return **data;
    }

    else throw "submatrix > matrix or < 1";

}

#endif // HEADER_H
